<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{config('app.name')}} - 关于我们</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel=stylesheet href=//s3.pstatp.com/toutiao/static/css/page/user_agreement/index.8d3b1046beb19da27fb2eefed389282c.css>
</head>
<body><div><div class="doc-container"><div class="doc-article"><section><p>“{{config('app.name')}}”于2012年8月上线，是一款基于数据挖掘技术的个性化推荐引擎产品，它为用户推荐有价值的、个性化的信息，提供连接人与信息的新型服务，是国内移动互联网领域成长最快的产品之一。</p> <p>在信息爆炸的时代，人们面对的选择越来越多，选择过多，信息超载，也常常会使人无所适从。在这种情况下，推荐引擎便开始展现技术优势，发挥威力。“{{config('app.name')}}”就是一款基于数据挖掘的推荐引擎产品，它不是传统意义上的新闻客户端，没有采编人员，不生产内容，运转核心是一套由代码搭建而成的算法。算法模型会记录用户在{{config('app.name')}}上的每一次行为，在海量的资讯里知道你感兴趣的内容，甚至知道你有可能感兴趣的内容，并将它们精准推送给你。</p> <p>“{{config('app.name')}}”推出了开放的内容创作与分发平台--“头条号”，是针对媒体、国家机构、企业以及自媒体推出的专业信息发布平台，致力于帮助内容生产者在移动互联网上高效率地获得更多的曝光和关注。截至2017年10月，“头条号”平台的账号数量已超过110万个。</p> <p>2012年，{{config('app.name')}}从边缘切入，经过6年的快速发展如今已经跻身新闻资讯客户端第一阵营。“{{config('app.name')}}”会继续努力，成为最懂你的信息平台，连接人与信息，促进创作和交流。</p> <p>请下载我们的主打应用<a href="/" target="_blank">{{config('app.name')}}（iPhone、Android版）</a>到你的手机上，不再错过任何你感兴趣的事！</p> <p>你关心的，才是头条！</p></section></div></div>
<div></body></html>