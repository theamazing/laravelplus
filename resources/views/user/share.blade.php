<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="font-size: 48px;">

<head>
    <title>立即领取现金</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta id="viewport" name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,viewport-fit=cover">
    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="noindex,follow">
    <link rel="stylesheet" type="text/css" href="vendor/share/css/index.css" />
</head>

<body>
    <div id="app">
        <div data-reactroot="" class="share-invite-lite" style="background-image: url(vendor/share/img/bg.jpg);">
            <!-- react-empty: 7 -->
            <div class="invite">
                <div style="overflow: hidden; height: 0.48rem;">
                    <div id="scroll" style="position: relative; height: 0.96rem; transition: transform 1000ms ease 0s;">
                        <a class="scroll-bar-item" href="javascript:;"><img class="fire-icon" alt="" src="vendor/share/img/laba.png">
                            <span class="item-text">小小文西***邀请9个好友，获得81元现金</span>
                        </a>
                        <a class="scroll-bar-item" href="javascript:;"><img class="fire-icon" alt="" src="vendor/share/img/laba.png">
                            <span class="item-text">TimK***邀请9个好友，获得81元现金</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="mentor"><span class="mertor-name"></span><span class="mertor-cong" style="color: white; font-size: 16px;">恭喜你获得</span></div>
            <div class="redpacket"><img class="redpacket-bg" src="vendor/share/img/02.png">
                <div class="map-area"></div>
                <div class="redpacket-inner">
                    <div class="redpacket-reward">
                        <div class="redpacket-reward-number">32</div>
                        <div class="redpacket-reward-unit">元</div><span class="popup">最高</span>
                    </div>
                    <div class="redpacket-tip">新人任务奖励</div>
                </div>
                <div class="my-invite-code">
                    <div id="action" class="invite-btn">立即下载领取</div>
                </div>
            </div>
        </div>
    </div>
</body>


<style type="text/css">
    .chang{
        transform: translate(0px, -0rem);
    }
    .changed{
        transform: translate(0px, -0.45rem);
    }
</style>

<script type="text/javascript">
    var dom = document.getElementById('scroll');
    function changeUser(){
        if (dom.className == 'chang') {
            dom.className = 'changed';
        }else{
            dom.className = 'chang';
        }
    }
    setInterval('changeUser()',2000); 
    document.getElementById('action').onclick = function(){
        if (window.location.href.indexOf('?')) {
            window.location.href += '&action=1';
        }else{
            window.location.href += '?action=1';
        }
    }
    
</script>

</html>