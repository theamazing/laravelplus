<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'users' => '现金提现',
    'check_ins'    => '签到福利',
    'check_ins_month' => '月签到福利',
    'user_footprints' => '阅读新闻',
    'user_task'   => '用户中心拓展任务',
    'newuser_task'   => '新手任务',
    'share_ten_news' => '分享文章福利',
    'daily_task' => '日常任务',
    'share_active_rewards' => '分享注冊活动奖励',
    'withdraw' => '收益提现',
    'loginBonus' => '累计登陆红包',
    'index_quan' => '首页新人券',
    'appstart_quan' => '天天红包',
    'user_quan' => '用户中心券'
];
