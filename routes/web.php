<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * 首页下载页
 */
Route::get('/', 'HomeController@share');

/**
 * 首页下载页
 */
Route::get('/share', 'HomeController@share');


/**
 * 关于我们 - |
 * 用户协议 - |
 * 隐私政策 - |
 */
Route::get('about', 'HomeController@about');
Route::get('user_agreement', 'HomeController@userAgreement');
Route::get('privacy_protection', 'HomeController@privacyProtection');


