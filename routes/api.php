<?php
use Illuminate\Routing\Router;

Route::group(
    [
        'prefix'        => config('api.route.prefix'),
        'namespace'     => config('api.route.namespace'),
    ],
    function () {
        /**
         * 处理基础路由
         */
        Route::group([], function (Router $router) {
            /**
             * -获取手机验证码
             */
            Route::post('phonevifer', 'UsersController@phoneVifer');

            /**
             * -注册用户
             */
            Route::post('register', 'UsersController@register');//新增用户


            /**
             * -用户登录
             */
            Route::post('login', 'UsersController@login');

            /**
             * -验证码登录
             */
            Route::post('logoinvifer', 'UsersController@logoinVifer');

            /**
             * -用户三方登录
             */
            Route::post('loginoauth', 'UsersController@loginOauth');

            /**
             * -用户登出
             */
            Route::post('signout', 'UsersController@signout')
                ->middleware('auth:api');

            /**
             * -重置用户密码
             */
            Route::post('resetpwd', 'UsersController@resetPwd');//重置密码

            /**
             * 统计活跃时间
             */
            Route::get('active', 'UsersController@sumLoginTime')->middleware('auth:api');

            /**判断是否手机是否注册过*/
            $router->get('newphone', 'UsersController@newPhone');
        });

        /**
         * 用户中心
         */
        Route::group(
            [
                'prefix' => 'user',
                'namespace' => 'Users',
                'middleware' => 'auth:api'
            ],
            function (Router $router) {
                /*修改用户的详细信息*/
                $router->get('info', 'UserInfoController@getUserInfo');
                /*修改用户的详细信息*/
                $router->post('update', 'UserInfoController@setUserInfo');
                /*用户实名认证*/
                $router->post('certification', 'UserInfoController@certification');
                /*查询实名认证消息*/
                $router->get('certification', 'UserInfoController@getCertification');
            }
        );

 
        /**
         * app中心
         */
        Route::group(
            [
                'prefix' => 'appinfo',
                'namespace' => 'AppInfo',
            ],
            function (Router $router) {
                /*Android 更新用接口*/
                $router->get('android/update', 'SystemController@androidVersion');

                /*iOS 版本更新接口*/
                $router->get('ios/update', 'SystemController@iosVersion');

                /*系统通知*/
                $router->get('notice', 'SystemController@notice');
                
                $router->post('configs', 'SystemController@configs');

                /*问题*/
                $router->get('question', 'SystemController@question');
                /*反馈*/
                $router->post('feedback', 'SystemController@feedback');
            }
        );
    }
);
