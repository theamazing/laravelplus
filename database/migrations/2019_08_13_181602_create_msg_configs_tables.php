<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsgConfigsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msg_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('msg_key',64)->comment('短信平台对应的key');
            $table->string('msg_id',64)->comment('短信平台对应的模板id');
            $table->string('msg_param',64)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msg_configs');
    }
}
