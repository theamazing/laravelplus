<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_infos');
        Schema::create('user_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->uniqid();
            $table->string('nickname', 20);
            $table->string('picture')->nullable();
            $table->unsignedTinyInteger('gender')->default(0)->comment('0:男；1:女');
            $table->unsignedTinyInteger('age')->default(0);
            $table->string('city')->nullable();
            $table->unsignedTinyInteger('bindvx_is')->default(0);
            $table->string('bindvx_name')->nullable();
            $table->string('invite_code', 8)->nullable();
            $table->string('uuid', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
