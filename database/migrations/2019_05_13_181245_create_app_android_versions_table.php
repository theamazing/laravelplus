<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppAndroidVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_android_versions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('version');
            $table->tinyInteger('force')->default(0)->comment('0不强制更新;1强制更新');
            $table->string('url');
            $table->text('notice');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_android_versions');
    }
}
