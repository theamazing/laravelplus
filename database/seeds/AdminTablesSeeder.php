<?php

use Illuminate\Database\Seeder;

class AdminTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // base tables
        Encore\Admin\Auth\Database\Menu::truncate();
        Encore\Admin\Auth\Database\Menu::insert(
            [
                [
                    "id" => 1,
                    "parent_id" => 0,
                    "order" => 1,
                    "title" => "App总览",
                    "icon" => "fa-bar-chart",
                    "uri" => "/",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 2,
                    "parent_id" => 0,
                    "order" => 2,
                    "title" => "后台设置",
                    "icon" => "fa-cogs",
                    "uri" => null,
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 3,
                    "parent_id" => 2,
                    "order" => 3,
                    "title" => "管理账号",
                    "icon" => "fa-users",
                    "uri" => "auth/users",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 4,
                    "parent_id" => 2,
                    "order" => 4,
                    "title" => "角色",
                    "icon" => "fa-user",
                    "uri" => "auth/roles",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 5,
                    "parent_id" => 2,
                    "order" => 5,
                    "title" => "权限",
                    "icon" => "fa-ban",
                    "uri" => "auth/permissions",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 6,
                    "parent_id" => 2,
                    "order" => 6,
                    "title" => "菜单",
                    "icon" => "fa-bars",
                    "uri" => "auth/menu",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 7,
                    "parent_id" => 2,
                    "order" => 7,
                    "title" => "日志",
                    "icon" => "fa-history",
                    "uri" => "auth/logs",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 8,
                    "parent_id" => 11,
                    "order" => 8,
                    "title" => "Storage文件",
                    "icon" => "fa-folder-open-o",
                    "uri" => "media",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 9,
                    "parent_id" => 11,
                    "order" => 9,
                    "title" => "Redis管理",
                    "icon" => "fa-database",
                    "uri" => "redis",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 10,
                    "parent_id" => 11,
                    "order" => 9,
                    "title" => "错误日志",
                    "icon" => "fa-exclamation-triangle",
                    "uri" => "logs",
                    "permission" => null,
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 11,
                    "parent_id" => 0,
                    "order" => 11,
                    "title" => "程序管理",
                    "icon" => "fa-code",
                    "uri" => null,
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 12,
                    "parent_id" => 0,
                    "order" => 12,
                    "title" => "用户管理",
                    "icon" => "fa-user",
                    "uri" => null,
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 13,
                    "parent_id" => 12,
                    "order" => 13,
                    "title" => "用户列表",
                    "icon" => "fa-users",
                    "uri" => "users",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 14,
                    "parent_id" => 0,
                    "order" => 14,
                    "title" => "App管理",
                    "icon" => "fa-rocket",
                    "uri" => null,
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 15,
                    "parent_id" => 14,
                    "order" => 15,
                    "title" => "Android版本",
                    "icon" => "fa-android",
                    "uri" => "app/android-version",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 16,
                    "parent_id" => 14,
                    "order" => 16,
                    "title" => "iOS版本",
                    "icon" => "fa-apple",
                    "uri" => "app/ios-version",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 17,
                    "parent_id" => 14,
                    "order" => 17,
                    "title" => "App公告",
                    "icon" => "fa-bullhorn",
                    "uri" => "app/notice",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
                [
                    "id" => 18,
                    "parent_id" => 14,
                    "order" => 18,
                    "title" => "App配置",
                    "icon" => "fa-cogs",
                    "uri" => "app/config",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ]
            ]
        );

        Encore\Admin\Auth\Database\Permission::truncate();
        Encore\Admin\Auth\Database\Permission::insert(
            [
                [
                    "id" => 1,
                    "name" => "All permission",
                    "slug" => "*",
                    "http_method" => "",
                    "http_path" => "*",
                    "created_at" => null,
                    "updated_at" => null
                ],
                [
                    "id" => 2,
                    "name" => "Dashboard",
                    "slug" => "dashboard",
                    "http_method" => "GET",
                    "http_path" => "/",
                    "created_at" => null,
                    "updated_at" => null
                ],
                [
                    "id" => 3,
                    "name" => "Login",
                    "slug" => "auth.login",
                    "http_method" => "",
                    "http_path" => "/auth/login\r\n/auth/logout",
                    "created_at" => null,
                    "updated_at" => null
                ],
                [
                    "id" => 4,
                    "name" => "User setting",
                    "slug" => "auth.setting",
                    "http_method" => "GET,PUT",
                    "http_path" => "/auth/setting",
                    "created_at" => null,
                    "updated_at" => null
                ],
                [
                    "id" => 5,
                    "name" => "Auth management",
                    "slug" => "auth.management",
                    "http_method" => "",
                    "http_path" => "/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs",
                    "created_at" => null,
                    "updated_at" => null
                ],
                [
                    "id" => 6,
                    "name" => "Logs",
                    "slug" => "ext.log-viewer",
                    "http_method" => null,
                    "http_path" => "/logs*",
                    "created_at" => "2019-03-22 01:14:53",
                    "updated_at" => "2019-03-22 01:14:53"
                ],
                [
                    "id" => 7,
                    "name" => "Media manager",
                    "slug" => "ext.media-manager",
                    "http_method" => null,
                    "http_path" => "/media*",
                    "created_at" => "2019-03-22 01:19:42",
                    "updated_at" => "2019-03-22 01:19:42"
                ],
                [
                    "id" => 8,
                    "name" => "Redis Manager",
                    "slug" => "ext.redis-manager",
                    "http_method" => null,
                    "http_path" => "/redis*",
                    "created_at" => "2019-03-22 14:04:18",
                    "updated_at" => "2019-03-22 14:04:18"
                ]
            ]
        );

        Encore\Admin\Auth\Database\Role::truncate();
        Encore\Admin\Auth\Database\Role::insert(
            [
                [
                    "id" => 1,
                    "name" => "Administrator",
                    "slug" => "administrator",
                    "created_at" => "2019-03-22 00:45:48",
                    "updated_at" => "2019-03-22 00:45:48"
                ]
            ]
        );

        // pivot tables
        DB::table('admin_role_users')->truncate();
        DB::table('admin_role_users')->insert(
            [
                [
                    "role_id" => 1,
                    "user_id" => 1,
                    "created_at" => null,
                    "updated_at" => null
                ]
            ]
        );

        DB::table('admin_role_permissions')->truncate();
        DB::table('admin_role_permissions')->insert(
            [
                [
                    "role_id" => 1,
                    "permission_id" => 1,
                    "created_at" => null,
                    "updated_at" => null
                ]
            ]
        );

        // create a user.
        Encore\Admin\Auth\Database\Administrator::truncate();
        Encore\Admin\Auth\Database\Administrator::create([
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'name'     => 'Administrator',
        ]);
        // finish
    }
}
