<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Encore\Admin\Auth\Database\Menu::where('id', '>', 18)->delete();
        Encore\Admin\Auth\Database\Menu::insert(
            [
                [
                    "id" => 19,
                    "parent_id" => 12,
                    "order" => 19,
                    "title" => "实名认证",
                    "icon" => "fa-certificate",
                    "uri" => 'certification',
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],
               
                [
                    "id" => 20,
                    "parent_id" => 0,
                    "order" => 20,
                    "title" => "短信管理",
                    "icon" => "fa-envelope-o",
                    "uri" => "",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ], 
                [
                    "id" => 21,
                    "parent_id" => 20,
                    "order" => 21,
                    "title" => "验证码记录",
                    "icon" => "fa-envelope-o",
                    "uri" => "phonevifer",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ], 
                [
                    "id" => 22,
                    "parent_id" => 20,
                    "order" => 22,
                    "title" => "短信设置",
                    "icon" => "fa-envelope-o",
                    "uri" => "msgconfigs",
                    "permission" => "*",
                    "created_at" => null,
                    "updated_at" => null,
                ],                         
             
            
               
            ]
        );
    }
}
