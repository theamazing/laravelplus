<?php

use App\Models\MsgConfig;
use Illuminate\Database\Seeder;

class MsgConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MsgConfig::insert(
            [
                [
                    'msg_key'=>'cf50eae62ef846f7843f39abdddadc2c',
                    'msg_id'=>'146177',
                    'msg_param'=>'',
                ],
            ]
        );
    }
}
