<?php

use Illuminate\Database\Seeder;

class AppConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\AppConfig::insert(
            [
                [
                    'name'=>'是否使用通知',
                    'mark'=>'enable_notice',
                    'value'=>1,
                    'introduce'=>'1启用通知；0不启用通知'
                ],
            ]
        );
    }
}
