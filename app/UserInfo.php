<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
        'updated_at',
        'deleted_at'
    ];

    //
    /**
     * 获得拥有此电话的用户。
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * 给头像赋值网址。
     *
     * @param  string  $value
     * @return string
     */
    public function getPictureAttribute($value)
    {
        if (($value == false) || (strpos($value, '//') !== false)) {
            return $value;
        }

        return asset('storage'.$value);
    }
}
