<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->resource('users', UserAdmin::class);
    $router->resource('app/android-version', AppAndroidVersionAdmin::class);
    $router->resource('app/ios-version', AppIosVersionAdmin::class);
    $router->resource('app/notice', AppNoticeAdmin::class);
    $router->resource('app/config', AppConfigAdmin::class);
    $router->resource('phonevifer', PhoneViferAdmin::class);
    //实名认证
    $router->resource('certification', Certificate::class);
    $router->get('certification/pass/{id}', 'Certificate@pass');
    $router->get('certification/notpass/{id}', 'Certificate@notPass');
  
    //公告管理
    $router->resource('notice-manage', NoticeManageController::class);
    //短信配置
    $router->resource('msgconfigs', MsgConfigAdmin::class);

});
