<?php

namespace App\Admin\Controllers;

use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserAdmin extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '会员管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);
        $grid->column('phone', __('会员账号'));
        $grid->info()->nickname('昵称');
        $grid->column('status', __('账户状态'))->using([0=>'启用',1=>'停用']);
        $grid->column('amount', __('账号余额'));
        $grid->info()->picture('头像');
        $grid->info()->gender('性别')->using([0=>'男',1=>'女']);
        $grid->column('created_at', __('创建时间'));

        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->equal('phone', '会员账号');
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->equal('status', '账号状态')->select([0=> '启用', 1 => '禁用']);
            });
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('会员Id'));
        $show->field('phone', __('会员账号'));
        $show->status('账号状态')->using([0=>'启用',1=>'停用']);
        $show->field('amount', __('账号余额'));       
        $show->field('created_at', __('创建时间'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->mobile('phone', __('会员账号'));
        $form->currency('amount', '用户余额')->symbol('￥/角')->default(0.00);
        $form->text('info.nickname', '会员昵称');
        $states = [
            'on'  => ['value' => 1, 'text' => '启用', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => '禁用', 'color' => 'danger'],
        ];
        $form->switch('status','账号状态')->states($states);
        $form->select('info.gender', '会员性别')->options([0 => '男', 1 => '女']);
        $form->number('info.age', '会员年纪')->min(1)->max(120);

        return $form;
    }
}
