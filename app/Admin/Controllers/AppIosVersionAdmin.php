<?php

namespace App\Admin\Controllers;

use App\Models\AppIosVersion;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AppIosVersionAdmin extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Ios版本管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AppIosVersion);
        $grid->column('version', __('更新版本'));
        $grid->column('force', __('强制更新'))->using([0 => '否', 1 => '是']);
        $grid->column('url', __('下载地址'));
        $grid->column('notice', __('更新公告'));
        $grid->column('created_at', __('时间'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AppIosVersion::findOrFail($id));
        $show->field('version', __('更新版本'));
        $show->field('force', __('强制更新'))->using([0 => '否', 1 => '是']);
        $show->field('url', __('下载地址'));
        $show->field('notice', __('更新公告'));
        $show->field('created_at', __('更新时间'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AppIosVersion);

        $form->text('version', __('更新版本'));
        $form->switch('force', __('强制更新'));
        $form->url('url', __('下载地址'));
        $form->textarea('notice', __('更新公告'));

        return $form;
    }
}
