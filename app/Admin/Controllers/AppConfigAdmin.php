<?php

namespace App\Admin\Controllers;

use App\Models\AppConfig;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AppConfigAdmin extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App接口参数配置';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AppConfig);
        $grid->column('name', __('名称'));
        $grid->column('mark', __('标记'));
        $grid->column('value', __('参值'))->using([0=>'不启用',1=>'启用']);
        $grid->column('introduce', __('说明'));
        $grid->column('created_at', __('更新时间'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AppConfig::findOrFail($id));
        $show->field('name', __('名称'));
        $show->field('mark', __('标记'));
        $show->field('value', __('参值'))->using([0=>'不启用',1=>'启用']);
        $show->field('introduce', __('说明'));
        $show->field('created_at', __('更新时间'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AppConfig);

        $form->text('name', __('名称'));
        $form->text('mark', __('标记'));
        $states = [
            'on'  => ['value' => 0, 'text' => '启用', 'color' => 'success'],
            'off' => ['value' => 1, 'text' => '禁用', 'color' => 'danger'],
        ];
        $form->switch('value','状态')->states($states);

        $form->text('introduce', __('说明'));
        return $form;
    }
}
