<?php

namespace App\Admin\Controllers;

use App\Models\MsgConfig;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MsgConfigAdmin extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '短信参数设置';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MsgConfig);
        $grid->column('msg_key', __('短信平台APPKEY'));
        $grid->column('msg_id', __('短信模板Id'));
        $grid->column('msg_param', __('其他参数'));
        $grid->column('created_at', __('添加时间'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MsgConfig::findOrFail($id));
        $show->field('msg_key', __('短信平台APPKEY'));
        $show->field('msg_id', __('短信模板Id'));
        $show->field('msg_param', __('其他参数'));
        $show->field('created_at', __('Created at'));
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MsgConfig);

        $form->text('msg_key', __('短信平台APPKEY'));
        $form->text('msg_id', __('短信模板Id'));
        $form->text('msg_param', __('其他参数'));
        return $form;
    }
}
