<?php

namespace App\Admin\Controllers;

use App\Models\PhoneVifer;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PhoneViferAdmin extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '验证码记录';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PhoneVifer);
        $grid->model()->orderby('id','desc');
        $grid->column('phone', __('手机号码'));
        $grid->column('action', __('操作'));
        $grid->column('code', __('验证码'));
        $grid->column('status', __('状态'))->using([0=>'有效',1=>'过期']);
        $grid->column('created_at', __('添加时间'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PhoneVifer::findOrFail($id));
        $show->field('phone', __('手机号码'));
        $show->field('action', __('操作'));
        $show->field('code', __('验证码'));
        $show->field('status', __('状态'))->using([0=>'有效',1=>'过期']);
        $show->field('created_at', __('添加时间'));
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PhoneVifer);
        $form->mobile('phone', __('手机号码'));
        $form->text('action', __('操作'));
        $form->text('code', __('验证码'));
        $form->switch('status', __('Status'));
        return $form;
    }
}
