<?php

namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Form;
use App\Models\RealNameCertification;

class Certificate extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Content $content)
    {
        return $content->header('实名认证')
            ->description('列表')
            ->body($this->grid());
    }


    public function grid()
    {
        $grid = new Grid(new RealNameCertification);
        $grid->id('ID');
        $grid->name('真实姓名');
        $grid->identity_number('身份证号');
        $grid->status('状态')->display(function ($status) {
            switch ($status) {
                case 1: $res = '待审核';break;
                case 2: $res = '通过';break;
                case 3: $res = '不通过';break;
            }
            return $res;
        });
        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableEdit();
        });
        return $grid;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Content $content)
    {
        return $content->header('实名认证')
            ->description('详情')
            ->body($this->detail($id));
    }

    public function detail($id)
    {
        $show = new Show(RealNameCertification::findorFail($id));
        $show->id('ID');
        $show->name('真实姓名');
        $show->identity_number('身份证号');
        $show->card_front('身份证正面')->image();
        $show->card_reverse('身份证反面')->image();
        $show->status('状态')->using([1=>'待审核',2=>'通过',3=>'不通过']);
        $show->panel()->tools(function ($tools) use ($id) {
            $tools->disableEdit();
            $tools->append('<a href="pass/'.$id.'" type="button" class="btn btn-sm btn-success" >
                            通过
                        </a><a href="notpass/'.$id.'" type="button" class="btn btn-sm btn-danger" >
                            不通过
                        </a>');
        });
        return $show;
    }

    /**
     * 审核通过
     */
    public function pass($id)
    {
        $RealNameCertification = RealNameCertification::findorFail($id);
        $RealNameCertification->status = 2;
        $res = $RealNameCertification->save();
        if ($res) {
            admin_success('审核成功');
            return redirect('/admin/certification');
        } else {
            admin_error('服务器错误，请重新审核');
            return redirect('/admin/certification/$id');
        }
    }
    /**
     * 审核通过
     */
    public function notPass($id)
    {
        $RealNameCertification = RealNameCertification::findorFail($id);
        $RealNameCertification->status = 3;
        $res = $RealNameCertification->save();
        if ($res) {
            admin_success('审核成功');
            return redirect('/admin/certification');
        } else {
            admin_error('服务器错误，请重新审核');
            return redirect('/admin/certification/$id');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return RealNameCertification::where('id', $id)->delete();
    }
}
