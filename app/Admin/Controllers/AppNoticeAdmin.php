<?php

namespace App\Admin\Controllers;

use App\Models\AppNotice;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AppNoticeAdmin extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App公告管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AppNotice);
        $grid->column('title', __('公告标题'));
        $grid->column('device', __('显示设备'));
        $grid->column('content', __('公告内容'));
        $grid->column('created_at', __('创建时间'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AppNotice::findOrFail($id));
        $show->field('title', __('公告标题'));
        $show->field('device', __('显示设备'));
        $show->field('content', __('公告内容'));
        $show->field('created_at', __('创建时间'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AppNotice);

        $form->text('title', __('公告标题'));
        $form->switch('device', __('显示设备'));
        $form->textarea('content', __('公告内容'));

        return $form;
    }
}
