<?php

/**
 * 这个函数用于将数组转换成xml文件;
 * @param $arr  数组格式数;
 */
use Illuminate\Support\Facades\DB;

function arrayToXml($arr)
{
    $xml = "<xml>";
    foreach ($arr as $key => $val) {
        $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
    }
    $xml .= "</xml>";
    return $xml;
}



/**
 * 发送http post 请求；
 * @sslCert 证书绝对路径；
 * @sslKey  密钥绝对路径；
 */
function curl_post_ssl($url, $data, $second = 30, $header = [], $sslCert = '', $sslKey = '')
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, $second);      //设置超时时间，默认为30秒
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //不验证主机
    // ssl证书
    if (!empty($sslCert)) {
        curl_setopt($ch, CURLOPT_SSLCERT, $sslCert);
    }
    // ssl密钥
    if (!empty($sslKey)) {
        curl_setopt($ch, CURLOPT_SSLKEY, $sslKey);
    }
    //设置请求头
    if (!empty($header)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $data = curl_exec($ch);
    $res = [];
    if ($data) {
        curl_close($ch);
        $res['status'] = 'S';
        $res['data'] = $data;
        return $res;
    } else {
        $res['status'] = 'F';
        $res['data'] = curl_error($ch);
        curl_close($ch);
        return $res;
    }
}

function getRandom($array, $rate, $flag=1)
{
    if ($flag) {
        $array = [$array[0]*100, $array[1]*100];
    }
    $rate = explode(':', $rate);
    $addend = floor(($array[1]-$array[0])/count($rate));
    for ($i=0; $i<count($rate)-1;$i++) {
        $arr[] = $array[0]+$addend*($i+1);
    }
    $arr[] = $array[1];

    $sum = 0;
    $left = 0;
    $right = 0;
    foreach ($rate as $value) {
        $sum+=$value*100;
    }
    $temp = rand(0, $sum);
    foreach ($rate as $key=>$value) {
        $right+=$value*100;
        if ($left<=$temp && $temp<$right) {
            $before = isset($arr[$key-1]) ? $arr[$key-1] : $array[0];

            if ($flag) {
                return number_format(rand($before, $arr[$key])/100, 2);
            }
            return rand($before, $arr[$key]);
        }
        $left+=$value*100;
    }
}

function getConfig($mark)
{
    if (!Cache::has('myConfig')) {
        $data = DB::table('app_configs')->all()->toArray();
        $new = [];
        foreach ($data as $val) {
            $new[$val['mark']] = $val;
        }
        Cache::put('myConfig', $new);
    } else {
        $new = Cache::get('myConfig');
    }

    return isset($new[$mark]) ? $new[$mark]['value'] : '';
}
