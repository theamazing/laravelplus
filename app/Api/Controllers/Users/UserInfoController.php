<?php

namespace App\Api\Controllers\Users;
use App\Models\RealNameCertification;
use App\Api\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use App\UserInfo;
class UserInfoController extends Controller
{

    /**
     * [获取用户信息]
     * @author pennilessfor@gmail.com
     * @date    2019-06-03
     * @version 1.0.0
     * @return  [type]     [description]
     */
    public function getUserInfo(Request $request)
    {
        $user = $request->user();
        $realName = RealNameCertification::where('user_id', $request->user()->id)->first();
        $msg = ['尚未实名认证','实名认证中','已实名认证','实名认证失败'];
        if (empty($realName)) {
            $user->verifyStatus=0;
            $user->verifyMsg=$msg[0];
        } else {
            $user->verifyStatus=$realName->status;
            $user->verifyMsg=$msg[$realName->status];
        }
        $user->info = $request->user()->info;
        return $this->resp200($user);
    }


    

    /**
     * [用户修改信息]
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   Request    $request [description]
     */
    public function setUserInfo(Request $request)
    {
        $this->validators($request->only('phone', 'password', 'age', 'picture'), [
            'nickname' => 'bail|min:3|max:20',
            'gender' => 'bail|min:1|max:2',
            'age' => 'bail|numeric|min:8|max:150',
            'picture' =>'bail|image|file',
        ]);
        //登录用户过的用户
        $info = UserInfo::where('user_id', $request->user()->id)->first();
        if (!$info) {
            $info = new UserInfo;
            $info->user_id = $request->user()->id;
            $info->nickname = "趣小白-";
        }
        //用户名
        if ($request->has('nickname')) {
            $info->nickname = $request->nickname;
        }
        //性别
        if ($request->has('gender')) {
            $info->gender = $request->gender;
        }
        //年龄
        if ($request->has('age')) {
            $info->age = $request->age;
        }
        //头像
        if ($request->hasFile('picture')) {
            $baseDir = config('filesystems.disks.public.root');

            $infoDir = DIRECTORY_SEPARATOR.
                            'avatar'.DIRECTORY_SEPARATOR.
                            $request->user()->phone.time().'.jpg';

            $info->picture = $infoDir;

            Image::make($request->file('picture'))
            ->resize(200, 200)
            ->save($baseDir.$infoDir);
        }

        if ($info->save()) {
            $info->user;
            return $this->resp201('更新完成！');
        }
        return $this->resp400('更新失败，请稍后！');
    }  

    /*
     * 实名认证
     */
    public function certification(Request $request)
    {
        $this->validators([
            'name' => 'required',
            'identity_number' => 'bail|required|digits_between:15,18',
            'card_front' => 'required|image',
            'card_reverse' => 'required|image'
        ]);
        $certification = RealNameCertification::where('user_id', $request->user()->id)->first();
        if ($certification) {
            if (in_array($certification->status, [0, 1])) {
                return $this->resp400('实名认证中或已实名！');
            }
        } else {
            $certification = new RealNameCertification();
        }
        $certification->user_id = $request->user()->id;
        $certification->name = $request->name;
        $certification->identity_number = $request->identity_number;

        if ($request->file('card_front')->isValid() && $request->file('card_reverse')->isValid()) {
            if (in_array(strtolower($request->file('card_front')->extension()), ['jpeg','jpg','gif','gpeg','png'])
                && in_array(
                    strtolower($request->file('card_front')->extension()),
                    ['jpeg','jpg','gif','gpeg','png']
                )) {
                $card_front = $request->card_front->store('id_card/'.date('Ym'), 'public');
                $certification->card_front = 'http://'.request()->server('HTTP_HOST').'/storage/'.$card_front;

                $card_reverse = $request->card_reverse->store('id_card/'.date('Ym'), 'public');
                $certification->card_reverse = 'http://'.request()->server('HTTP_HOST').'/storage/'.$card_reverse;
            } else {
                return $this->resp400('上传图片格式错误！');
            }
        } else {
            return $this->resp400('上传错误！');
        }
        $certification->status = 1;
        if ( $certification->save()) {           
            return $this->resp201('上传成功！');
        } else {
            return $this->resp400('上传失败！');
        }
    }


}
