<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * 对返回的数据进行格式输出
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   integer    $code      [description]
     * @param   [type]     $arguments [description]
     * @return  [type]                [description]
     */
    public function resp($code = 200, $arguments)
    {
        $res['status'] = (int)$code;
        if (count($arguments) == 1) {
            if (is_string($arguments[0])) {
                $res['result'] = null;
                $res['message'] = $arguments[0] ?? null;
            } else {
                $res['result'] = $arguments[0] ?? null;
                $res['message'] = null;
            }
        } else {
            $res['result'] = $arguments[0] ?? null;
            $res['message'] = $arguments[1] ?? null;
        }
        return response()->json($res);
    }

    /**
     * 数据验证拦截
     * @param  array 验证的数据
     * @param  array 验证的规则
     * @return action 验证失败就拦截输出
     */
    public function validators($data = [], $checkd = [])
    {
        $valid = validator($data, $checkd);
        if ($valid->fails()) {
            $res['status'] = 412;
            $res['message'] = $valid->errors()->first();
            exit(json_encode($res));
        }
        return true;
    }

    public function __call($name, $arguments)
    {
        //
        $function = explode('resp', $name);
        if (isset($function[1])) {
            return $this->resp($function[1], $arguments);
        }
    }
}
