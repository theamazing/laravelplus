<?php

namespace App\Api\Controllers;

use App\Models\InviteCode;
use App\Models\UserShare;
use App\Models\UserShareActivity;
use App\User;
use App\UserInfo;
use App\Models\PhoneVifer;
use App\Models\UserOauth;
use App\Models\UserTask;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
//use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * [发送手机验证码]
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   Request    $request [description]
     * @return  [type]              [description]
     */
    public function phoneVifer(Request $request)
    {
        $this->validators($request->only('phone', 'action'), [
            'phone' => 'bail|required|digits:11',
            'action' => 'required|in:reg,res',
        ]);
        $code = rand(100000, 999999);
        if (PhoneVifer::sendVifer($request->phone, $request->action, $code, $msg)) {
            return $this->resp201('发送成功，请查收！');
        }
        return $this->resp400($msg);
    }

    /**
     * [创建用户]
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   Request    $request [description]
     */
    public function register(Request $request)
    {
        $this->validators($request->only('phone', 'password', 'phonevifer'), [
            'phone' => 'bail|required|digits:11',
            'password' => 'required|min:6|max:30',
            'phonevifer' => 'bail|required|digits:6',
        ]);
        // if (! PhoneVifer::viferStatus($request->phone, $request->phonevifer)) {
        //     return $this->resp400('验证码错误!');
        // }
        if (User::where('phone', $request->phone)->first()) {
            return $this->resp400('用户已经注册!');
        }
        $user = User::createUser([
            'name' => config('api.appName').'-'.time(),
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'uuid' => $request->uuid,
        ]);
        $user->token = $user->createToken('app')->accessToken;
        $user->info;
        return $this->resp200($user);
    }

    /**
     * [用手机号 密码进行用户认证]
     * @param  Request $request [账号密码]
     * @return [type]           [用户信息]
     */
    public function login(Request $request)
    {
        $this->validators($request->only('phone', 'password'), [
            'phone' => 'bail|required|digits:11',
            'password' => 'required|min:6|max:30',
        ]);
        if (
            ($user = User::where('phone', $request->phone)->where('status',0)
            ->first())  &&
            (Hash::check($request->password, $user->password))
        ) {
            $user->token = $user->createToken('app')->accessToken;
            $user->info;
            return $this->resp201($user, '登录成功');
        } else {
            return $this->resp400('请检查账号，或者密码！');
        }
    }

    /**
     * 处理用户注销登录逻辑
     * @author pennilessfor@gmail.com
     * @date    2019-06-21
     * @version 1.0.0
     * @param   Request    $request [description]
     * @return  [type]              [description]
     */
    public function signout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->resp200('退出成功');
    }

    /**
     * [用户通过手机验证码重置密码]
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   Request    $request [description]
     * @return  [type]              [description]
     */
    public function resetPwd(Request $request)
    {
        $this->validators($request->only('phone', 'password', 'phonevifer'), [
            'phone' => 'bail|required|digits:11',
            'password' => 'required|min:6|max:30',
            'phonevifer' => 'bail|required|digits:6',
        ]);
        if (! PhoneVifer::viferStatus($request->phone, $request->phonevifer)) {
            return $this->resp400('验证码错误!');
        }
        if (($user = User::where('phone', $request->phone)->first())) {
            $user->password = Hash::make($request->password);
            $user->save();
            return $this->resp201('修改完成');
        } else {
            return $this->resp400('用户不存在！');
        }
    }

    /**
     * [创建用户-同时绑定三方账户]
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   Request    $request [description]
     */
    public function logoinVifer(Request $request)
    {
        $this->validators($request->only('phone', 'password', 'phonevifer'), [
            'phone' => 'bail|required|digits:11',
            'phonevifer' => 'bail|required|digits:6',
        ]);

        if (! PhoneVifer::viferStatus($request->phone, $request->phonevifer)) {
            return $this->resp400('验证码错误!');
        }

        $user = User::where('phone', $request->phone)->first();
        if ($user) {
            if ($request->oauth_id) {
                $oauth = UserOauth::find($request->oauth_id);
                //绑定微信到已有用户
                if ($oauth->access_token == $request->access_token) {
                    UserOauth::userToOauth($oauth, $user);
                    $bindVX = 1;
                }
            }
        } else {
            //注册用户 同时绑定到三方账户资料以三方账户为主。
            if ($request->oauth_id) {
                $oauth = UserOauth::find($request->oauth_id);
                if ($oauth->access_token == $request->access_token) {
                    $user = new User;
                    $user->phone = $request->phone;
                    $user->password = Hash::make($request->password);
                    $user->save();
                    UserOauth::oauthToUser($oauth, $user);
                    $bindVX = 1;
                }
            } else {
                //普通注册账户
                $user = User::createUser([
                    'name' => '趣小白-'.time(),
                    'phone' => $request->phone,
                    'password' => Hash::make('123!@#qwe'),
                    'uuid' => $request->uuid,
                ]);
            }
        }
        $user->token = $user->createToken('app')->accessToken;
        $user->info;
        return $this->resp200($user);
    }

    /**
     * [三方登录整合]
     * @author pennilessfor@gmail.com
     * @date    2019-05-24
     * @version 1.0.0
     * @param   Request    $request [description]
     * @return  [type]              [description]
     */
    public function loginOauth(Request $request)
    {
        $this->validators($request->all(), [
            'token' => 'bail|required|json',
            'website' => 'required|in:wechat',
        ]);

        $token = json_decode($request->token, true);

        if ($request->website == 'wechat') {
            $access_token = $token['access_token'];
            $openid = $token['openid'];
            $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openid}&lang=zh_CN";
            $response = Curl::to($url)->asJson()->get();
            if (isset($response->errcode)) {
                return $this->resp400('微信登录失败错误信息为{'.$response->errmsg.'}');
            } elseif (isset($response->unionid)) {
                return $this->oauth_wechat($request, $response);
            } else {
                return $this->resp400('异常,已经记入日志!');
            }
        }
    }

    /**
     * 拆分处理微信登录
     * @author pennilessfor@gmail.com
     * @date    2019-06-12
     * @version 1.0.0
     * @param   Request    $request [description]
     * @param   [type]     $res     [description]
     * @return  [type]              [description]
     */
    private function oauth_wechat(Request $request, $res)
    {
        $exit = UserOauth::where('website', $request->website)
        ->where('unionid', $res->unionid)
        ->first();
        if ($exit && ($exit->user_id > 0)) {
            $user = user::find($exit->user_id);
            $user->token = $user->createToken($request->website)->accessToken;
            $user->info;
            return $this->resp201($user, '登录成功');
        } elseif ($exit && ($exit->user_id == 0)) {
        } else {
            $exit = new UserOauth;
            $exit->website = $request->website;
            $exit->access_token = (json_decode($request->token, true)['access_token']);
            $exit->openid = (json_decode($request->token, true)['openid']);
            $exit->unionid = $res->unionid;
            $exit->user_id = 0;
            $exit->oauth_user = json_encode($res);
            $exit->save();
        }
        if (Auth::guard('api')->check()) {
            $user = user::find(Auth::guard('api')->id());
            UserOauth::userToOauth($exit, $user);
            $usershare = UserShare::where('reg_id', $user->id)->first();
            if ($usershare) {
                $usershare->status = 1;
                $usershare->save();
            }
            $user->token = $user->createToken('app')->accessToken;
            $user->info;
            return $this->resp201($user, '绑定成功');
        }

        return $this->resp302([
            'oauth_id'=>$exit->id,
            'access_token'=>$exit->access_token
        ]);
    }


    /**
    * 统计app每天活跃时间
    */
    public function sumLoginTime(Request $request)
    {
        $userId = $request->user()->id;
        $startTimeCacheKey = "{$userId}startTime";
        $startTime=Cache::get($startTimeCacheKey, function () use ($startTimeCacheKey) {
            return time();
        });
        $activeTimeCacheKey = "{$userId}activeTime";
        if (Cache::has($activeTimeCacheKey)) {
            Cache::increment($activeTimeCacheKey, time()-$startTime);
            Cache::put($startTimeCacheKey, time(), 5*60);
        } else {
            Cache::put($activeTimeCacheKey, 0, Carbon::now()->endOfDay());
            Cache::put($startTimeCacheKey, time(), 5*60);
        }
        return $this->resp200(Cache::get($activeTimeCacheKey, 1), '累计在线时间');
    }

    /**
     * 是否新注册手机
     */
    public function newPhone(Request $request)
    {
        $this->validators($request->only('uuid'), [
            'uuid' => 'required'
        ]);
        $userInfo = UserInfo::where('uuid', $request->uuid)->first();
        if ($userInfo) {
            return $this->resp200(['status'=>0, 'msg'=> '该设备已注册过']);
        } else {
            return $this->resp200(['status'=>1, 'msg'=> '新设备可以分享注册']);
        }
    }
}
