<?php

namespace App\Api\Controllers\AppInfo;

use Illuminate\Http\Request;
use App\Api\Controllers\Controller;

use App\Models\AppAndroidVersion;
use App\Models\AppIosVersion;
use App\Models\AppNotice;
use App\Models\AppConfig;
use App\Models\Question;
use App\Models\Feedback;
use Illuminate\Support\Facades\Cache;

class SystemController extends Controller
{
    /**
     * android 版本升级
     * @author pennilessfor@gmail.com
     * @date    2019-05-14
     * @version 0.0.1
     * @return  [type]     [description]
     */
    public function androidVersion()
    {
        return $this->resp200(AppAndroidVersion::getLastVersion());
    }

    /**
     * ios 版本升级
     * @author pennilessfor@gmail.com
     * @date    2019-05-14
     * @version 0.0.1
     * @return  [type]     [description]
     */
    public function iosVersion()
    {
        return $this->resp200(AppIosVersion::getLastVersion());
    }

    /**
     * 系统公告
     * @author pennilessfor@gmail.com
     * @date    2019-05-14
     * @version 0.0.1
     * @return  [type]     [description]
     */
    public function notice()
    {
        if (!AppConfig::enableNotice()) {
            return $this->resp401();
        }
        $res = AppNotice::orderby('id', 'desc')->first();
        return $this->resp200($res?:[]);
    }

    /**
     * app配置文件
     * @author pennilessfor@gmail.com
     * @date    2019-05-14
     * @version 0.0.1
     * @return  [type]     [description]
     */
    public function configs(Request $request)
    {
        $this->validators($request->only('key'), [
            'key' => 'required',
        ]);        
        return $this->resp200(AppConfig::where('mark', $request->input('key'))->first());
    }

    /**
     * 问题反馈-问题
     * @author yeyork2019@gmail.com
     * @date    2019-07-12
     * @version 0.0.1
     * @return  [type]     [description]
     */
    public function question()
    {
        $question = Cache::remember(__FUNCTION__,60*5,function(){
            return Question::whereNull('deleted_at')->get();
        });
        return $this->resp200($question);
    }

    /**
     * 问题反馈-反馈
     * @author yeyork2019@gmail.com
     * @date    2019-07-12
     * @version 0.0.1
     * @return  [type]     [description]
     */
    public function feedback(Request $request)
    {
        $this->validators($request->only('question_id', 'content', 'contact'), [
            'question_id' => 'bail|required|numeric',
            'content' =>'required',
            'contact' =>'bail|required'
        ]);
        $feedback = new Feedback();
        $feedback->question_id = $request->question_id;
        $feedback->content = $request->content;
        $feedback->contact = $request->contact;
        $feedback->save();

        return $this->resp200(1, '提交反馈成功！');
    }
}
