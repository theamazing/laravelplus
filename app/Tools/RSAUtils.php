<?php
namespace App\Tools;

class RSAUtils
{
    /**
     * 获取私钥
     * @return bool|resource
     */
    private static function getPrivateKey($path)
    {
        $content = file_get_contents($path);
        return openssl_pkey_get_private($content);
    }

    /**
     * 获取公钥
     * @return bool|resource
     */
    private static function getPublicKey($path)
    {
        $content = file_get_contents($path);
        return openssl_pkey_get_public($content);
    }

    /**
     * 私钥加密
     * @param string $data
     * @return null|string
     */
    public static function privEncrypt($data = '', $path)
    {
        if (!is_string($data)) {
            return null;
        }
        $privateKey = self::getPrivateKey($path);
        return openssl_private_encrypt($data, $encrypted, $privateKey) ? base64_encode($encrypted) : null;
    }

    /**
     * 公钥加密
     * @param string $data
     * @return null|string
     */
    public static function publicEncrypt($data = '', $keyPath)
    {
        if (!is_string($data)) {
            return null;
        }
        $pubKey =  self::getPublicKey($keyPath);
        return openssl_public_encrypt($data, $encrypted, $pubKey) ? base64_encode($encrypted) : null;
    }

    /**
     * 私钥解密
     * @param string $encrypted
     * @return null
     */
    public static function privDecrypt($encrypted = '', $path)
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $privateKey = self::getPrivateKey($path);
        return (openssl_private_decrypt(base64_decode($encrypted), $decrypted, $privateKey)) ? $decrypted : null;
    }

    /**
     * 公钥解密
     * @param string $encrypted
     * @return null
     */
    public static function publicDecrypt($encrypted = '', $path)
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $pubKey = self::getPublicKey($path);
        return (openssl_public_decrypt(base64_decode($encrypted), $decrypted, $pubKey)) ? $decrypted : null;
    }
}
