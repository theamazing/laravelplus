<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    // use Notifiable;
    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname', 'phone','amount'
        // 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'deleted_at'
    ];

    /**
     * crate user
     * @param  array  $arr [add user]
     * @return [boole]      [user obj]
     */
    public static function createUser($arr = [])
    {
        $new = new User();
        $new->phone = $arr['phone'];
        $new->password = $arr['password'];
        if (!$new->save()) {
            return false;
        }
        $newInfo = new UserInfo();
        $newInfo->user_id = $new->id;
        $newInfo->nickname = $arr['name'];
        $newInfo->uuid = $arr['uuid'];
        if (!$newInfo->save()) {
            Log::warning('User.Model Row 53 || data:'.$new.']');
        }
        return $new;
    }

    public function info()
    {
        return $this->hasOne('App\UserInfo');
    }

}
