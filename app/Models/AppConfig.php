<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppConfig extends Model
{
    /**
     * 是否不弹出系统公告
     * @author pennilessfor@gmail.com
     * @date    2019-06-18
     * @version 1.0.0
     * @return  [type]     [description]
     */
    public static function enableNotice()
    {
        $res = self::where('mark', 'enable_notice')->first();

        if (isset($res->value) && $res->value == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkIns()
    {
        $cList = AppConfig::where('mark', 'checkin')->first();
        if (isset($cList->value)) {
            $moneyList = explode('_', $cList->value);
            if (count($moneyList) != 7) {
                exit('签到系统配置错误COUNT_NUMBER'.count($moneyList));
            }
        } else {
            exit('请联系后台管理员处理开放签到系统。- checkin');
        }
        return $moneyList;
    }
}
