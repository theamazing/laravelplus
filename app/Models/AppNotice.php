<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppNotice extends Model
{
    protected $hidden = [
        // "id",
        // "device",
        "deleted_at",
        "updated_at"
    ];
    //
}
