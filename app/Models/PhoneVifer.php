<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class PhoneVifer extends Model
{
    use SoftDeletes;

    /**
     * 处理验证手机验证码。
     * @author pennilessfor@gmail.com
     * @date    2019-06-20
     * @version 1.0.0
     * @param   [string]     $phone    [手机号]
     * @param   [string]     $code     [验证码]
     * @return  [string]               [成功或失败]
     */
    public static function viferStatus($phone, $code)
    {
        $last = self::where('phone', $phone)
        ->where('code', $code)->where('status', 0)
        ->where('created_at', '>', date("Y-m-d H:i:s", (time() - 1200)))
        ->first();
        if ($last) {
            $last->status = 1;
            $last->save();
            return true;
        }
        return false;
    }

    /**
     * 发送短信请求
     * @param  [int] $phone  [手机号]
     * @param  [str] $action [用途]
     * @param  [int] $code   [验证码]
     * @return [bool]         发送状态
     */
    public static function sendVifer($phone, $action, $code, &$msg = '')
    {
        $today = date("Y-m-d 00:00:00", time());
        $smsInfo = self::where('phone', $phone)->where('created_at', '>', $today)
            ->limit(5)->get();
        $smsCount = count($smsInfo);
        if ($smsCount > 4) {
            $msg = '今日发送已达上限';
            return false;
        }

        if ($smsCount >= 1) {
            $now = (int) time();
            $last = (int) strtotime($smsInfo[0]->created_at);
            $meet = $now - $last;
            $limit = 160;
            if ($meet < $limit) {
                $msg = '操作频繁，请'.($limit - $meet).'秒后重试！';
                return false;
            }
        }
        $config = Cache::remember(__FUNCTION__,5*60,function(){
            return $config = MsgConfig::first();
        });
        $smsConf = array(
            'key'   => $config['msg_key'], //您申请的APPKEY
            'mobile'    => $phone, //接受短信的用户手机号码
            'tpl_id'    => $config['msg_id'], //您申请的短信模板ID，根据实际情况修改
            'tpl_value' =>'#code#='.$code //您设置的模板变量，根据实际情况修改
        );
       
        $resStatus = PhoneVifer::curl('http://v.juhe.cn/sms/send', $smsConf, 1); //请求发送短信
        
        if ($resStatus) {
            $new = new PhoneVifer();
            $new->phone = $phone;
            $new->action = $action;
            $new->code = $code;
            $new->status = 0;
            $new->save();
            return true;
        } else {
            return false;
        }
    }

    /* 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    public static function curl($url, $params=false, $ispost=0)
    {
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                curl_setopt($ch, CURLOPT_URL, $url.'?'.$params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        $response = curl_exec($ch);
        if ($response === false) {
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);

        if ($response) {
            $result = json_decode($response, true);
            $error_code = $result['error_code'];
            if ($error_code == 0) {
                return true;
            } else {
                Log::warning("短信发送失败[(http code :".$error_code.")".curl_error($ch)."] request:".json_encode($params));
            }
        }
        return false;
    }
}
