<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppAndroidVersion extends Model
{
    /**
     * 获取最后的发布版本
     * @author pennilessfor@gmail.com
     * @date    2019-06-22
     * @version 1.0.0
     * @return  [type]     [description]
     */
    public static function getLastVersion()
    {
        return self::orderby('id', 'desc')->first();
    }
}
