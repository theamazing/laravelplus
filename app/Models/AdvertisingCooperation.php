<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertisingCooperation extends Model
{
    protected $fillable = [
        'user_id' ,
        'company' ,
        'addr'    ,
        'contact' ,
        'phone'
    ];
}
