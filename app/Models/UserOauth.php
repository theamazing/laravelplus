<?php

namespace App\Models;

use App\UserInfo;
use Illuminate\Database\Eloquent\Model;

class UserOauth extends Model
{

    /**
     * 绑定三方账号并同步信息到本地-新增用户.
     * @author pennilessfor@gmail.com
     * @date    2019-06-12
     * @version 1.0.0
     * @param   [type]     $oauth [description]
     * @param   [type]     $user  [description]
     * @return  [type]            [description]
     */
    public static function oauthToUser($oauth, $user)
    {
        $oauth->user_id = $user->id;
        $oauth->save();
        $oauthUser = json_decode($oauth->oauth_user, true);

        $newInfo = new UserInfo();
        $newInfo->user_id = $user->id;

        $newInfo->nickname = $oauthUser['nickname']??'趣小白';
        $newInfo->gender = $oauthUser['sex'] ?? 0 ;
        $newInfo->picture = $oauthUser['headimgurl'] ?? '';
        $newInfo->city = $oauthUser['city'] ?? '';
        $newInfo->invite_code = self::inviteCode();
        $newInfo->uuid = request()->uuid;

        // 三方账号的绑定信息
        $newInfo->bindvx_is = 1;
        $newInfo->bindvx_name =  $oauthUser['nickname']??'未取到微信昵称';

        if (!$newInfo->save()) {
            Log::warning('UserOauth.Model data:'.$user.']');
        }
    }
    public static function inviteCode()
    {
        $codeArr = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $str = '';
        $flag = 1;
        while ($flag) {
            for ($i=0;$i<8;$i++) {
                $str .= $codeArr[rand(0, 35)];
            }
            $data = UserInfo::where('invite_code', $str)->first();
            if (!$data) {
                $flag = 0;
            } else {
                $str = '';
            }
        }

        return $str;
    }

    /**
     * 仅仅绑定三方账号,不同步数据
     * @author pennilessfor@gmail.com
     * @date    2019-06-12
     * @version 1.0.0
     * @param   [type]     $oauth [description]
     * @param   [type]     $user  [description]
     * @return  [type]            [description]
     */
    public static function userToOauth($oauth, $user)
    {
        $oauth->user_id = $user->id;
        $oauth->save();

        $oauthUser = json_decode($oauth->oauth_user, true);
        $newInfo = UserInfo::where('user_id', $user->id)->first();
        $newInfo->bindvx_is = 1;
        $newInfo->bindvx_name =  $oauthUser['nickname']??'未取到微信昵称';
        $newInfo->save();
    }
}
