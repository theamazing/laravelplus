<?php

namespace App\Http\Middleware;

use Closure;

class AdminHostCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $had = in_array(
            $request->server('HTTP_HOST'),
            config('admin.route.hostname')
        );
        if (!$had && !config('app.debug')) {
            return redirect('/');
        }
        return $next($request);
    }
}
