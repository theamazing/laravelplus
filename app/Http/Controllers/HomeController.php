<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\News;

use Jenssegers\Agent\Agent;
use App\Models\AppAndroidVersion;
use App\Models\AppIosVersion;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    private $time_out = 3600;

    /**
     * 关于我们
     */
    public function about()
    {
        return view('system/about');
    }

    /**
     * 用户协议
     * @return [str] [view]
     */
    public function userAgreement()
    {
        return view('system/UserAgreement');
    }

    /**
     * 隐私政策
     * @return [str] [view]
     */
    public function privacyProtection()
    {
        return view('system/PrivacyProtection');
    }

    public function share()
    {
        return view('welcome');
    }
    
}
